var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

var base_url = 'http://localhost:3000/api/bicicletas';

describe('Bicicleta API', () => {
    beforeEach(function (done) {
        var mongoDB = 'mongodb://localhost/testdb'
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
    
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB connection error: '));
        db.once('open', function () {
            console.log('We are connected to test database!');
            done();
        });

    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function (error, success) {  
            if(error) console.log('error');
            done();
        });   
    });

    describe('GET BICICLETAS /', () => {
        it('Status 200', (done) => {
            request.get(base_url, function (error, response, body) {
                var result = JSON.parse(body); 
                expect(response.statusCode).toBe(200);            
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });
    
    describe("POST BICICLETAS /create", ()=> {
        it("Status 200", (done)=> {
            var headers = {'content-type': 'application/json'};
            var aBici = '{"id":3,"color":"green","model":"ruta","lat":6.852403,"lng":-75.439928}';
            request.post({
                headers: headers,
                url: urlServer + '/create',
                body: aBici
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe("green");
                expect(bici.localitation[0]).toBe(6.852403);
                expect(bici.localitation[1]).toBe(-75.439928);
                done();
            });
        });
    });
});