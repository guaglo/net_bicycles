var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function() {
    beforeAll((done) => { mongoose.connection.close(done) });

    beforeAll(function(done){
        var mongoDB = 'mongodb://localhost/testdb'
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('We are connected to test DB!!!');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done(); 
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de Bicicleta', () => {
          var bici = Bicicleta.createInstance(1,"green","ruta", [-34.5, -75.1]);
    
          expect(bici.code).toBe(1);
          expect(bici.color).toBe("green");
          expect(bici.modelo).toBe("ruta");
          expect(bici.ubicacion[0]).toEqual(-34.5);
          expect(bici.ubicacion[1]).toEqual(-75.1);
        });
    });

    /*
    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', (done)  => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });
    
    describe('Bicicleta.add', () => {
        it('agrega solo una bici', (done) => {
            var aBici = new Bicicleta({code: 1, color:"green", modelo:"mountain"});
            Bicicleta.add(aBici, function(err, newBici){
                if ( err ) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();
                });
            });
        });    
    });

    describe('Bicicleta.findByCode',() => {
        it('devuleve la bici con code 1', (done) => {
            Bicicleta.allBicis(function (err, bicis) {  
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbano"});
                Bicicleta.add(aBici, function(err, newBici){
                    if (err) console.log(err);

                    var aBici2 = new Bicicleta({code: 2, color: "gray", modelo: "ruta"});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if (err) console.log(err);
                        Bicicleta.findByCode(1,function(error, targetBici){
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);
                            done();
                        }); 
                    });
                });   
            })   
        });        
    });

    describe('Bicicleta.removeByCode',() => {
        it('delimina la bici con code 1', (done) => {
            Bicicleta.allBicis(function (err, bicis) { 
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbano"});
                Bicicleta.add(aBici, function(err, newBici){
                    if (err) console.log(err);

                    var aBici2 = new Bicicleta({code: 2, color: "gray", modelo: "ruta"});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if (err) console.log(err);

                        Bicicleta.allBicis(function (err, bicis) {
                            expect(bicis.length).toBe(2);

                            Bicicleta.removeByCode(1,function(err,targetBici){
                                done();    
                            }); 
                        }); 
                    });
                });   
            })   
        });        
    });*/

});


/*
beforeEach(()=>{
    Bicicleta.allBicis = [];
});

describe('Bicicleta.allBicis', () => {
    it('comienza vacio', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add', () => {
    it('agregamos una bici', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, 'red', 'mountain', [6.292104, -75.598431]);
        Bicicleta.add(a);
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);

    });
});

describe('Bicicleta.findById', () => {
    it('devuelve la bici con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        
        var b = new Bicicleta(1, 'green light', 'ruta', [6.292104, -75.598431]);
        var c = new Bicicleta(2, 'white', 'mountain',[6.252104, -75.568431]);
        Bicicleta.add(b);
        Bicicleta.add(c);

        var busquedabici = Bicicleta.findById(1);
        expect(busquedabici.id).toBe(1);
        expect(busquedabici.color).toBe(b.color);
        expect(busquedabici.modelo).toBe(b.modelo);
    });
});

describe("Bicicleta.removeById", () => {
    it('borrado por ID', () => {
        
        expect(Bicicleta.allBicis.length).toBe(0);
        
        var d = new Bicicleta(1, 'green light', 'ruta', [6.292104, -75.598431]);
        var e = new Bicicleta(2, 'white', 'mountain',[6.252104, -75.568431]);
        Bicicleta.add(d);
        Bicicleta.add(e);
        expect(Bicicleta.allBicis.length).toBe(2);
        
        Bicicleta.removeById(2);
        expect(Bicicleta.allBicis.length).toBe(1);
    });
});*/
